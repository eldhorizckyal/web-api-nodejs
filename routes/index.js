var express = require('express');
var router =  express.Router();
var connection  = require('../database');
const mysql = require('mysql');

//tampilkan semua data movies
router.get('/api/movies',(req, res) => {
    let sql = "SELECT * FROM movies";
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});
 
//tampilkan data movies berdasarkan id
router.get('/api/movies/:id',(req, res) => {
    let sql = "SELECT * FROM movies WHERE id="+req.params.id;
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});

//tampilkan semua data cast
router.get('/api/casts',(req, res) => {
    let sql = "SELECT * FROM casts";
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
  });
   
//tampilkan data cast berdasarkan id
router.get('/api/casts/:id',(req, res) => {
    let sql = "SELECT * FROM casts WHERE id="+req.params.id;
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});

//tampilkan semua data movie dan cast
router.get('/api/detailmovie',(req, res) => {
    let sql = "SELECT * FROM detailmovie";
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
  });

//tampilkan data movie dan cast berdasarkan id
router.get('/api/detailmovie/:id',(req, res) => {
    let sql = "SELECT * FROM detailmovie WHERE id="+req.params.id;
    let query = connection.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});
 
//Tambahkan data movies baru
router.post('/api/movies',(req, res) => {
    let data = {name: req.body.name, language: req.body.language, status: req.body.status, rating: req.body.rating};
    let sql = "INSERT INTO movies SET ?";
    let query = connection.query(sql, data,(err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});

//Tambahkan data casts baru
router.post('/api/casts',(req, res) => {
    let data = {name: req.body.name, birthday: req.body.birthday, deadday: req.body.deadday, rating: req.body.rating};
    let sql = "INSERT INTO casts SET ?";
    let query = connection.query(sql, data,(err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
    });
});

module.exports = router;