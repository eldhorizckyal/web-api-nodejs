const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var router =  express.Router();

//Untuk parse file json
app.use(bodyParser.json());

//Pakai routes untuk buka page 
var pageRoutes =  require('./routes/index.js');
app.use(pageRoutes);

//Server jalan di port 3000
app.listen(3000,() =>{
  console.log('Server sudah jalan di port 3000');
});